#!/usr/bin/python
import sys
from mutators import *
from problems import problems
from utils.command import TimerTask

def showUsage(problems, mutators):
	print 'Usage is: \t{0} <problem> [<experiment>]\n'.format(sys.argv[0])

	print 'The problem can be \'all\' or a specific problem.'
	print 'The experiment can be \'time\' or a mutator. If using \'time\' you also have to provide an extra argument with the size of the test\'s suite.'
	print 'Available problems: \t{0}'.format([x.get_short_name() for x in problems])
	print 'Available mutators: \t{0}'.format([x.get_short_name() for x in mutators])

if __name__ == "__main__":
	mkdir_cmd = TimerTask('mkdir -p logs')
	mkdir_cmd.run()

	if len(sys.argv) == 1:
		showUsage(problems, mutators)
		exit(1)
	else:
		problem_name = sys.argv[1]
		problems_to_process = []
		if problem_name == 'all':
			problems_to_process  = problems[:]
		else:
			for p in problems:
				if p.get_short_name() == problem_name:
					problems_to_process.append(p)
					break
			if len(problems_to_process) == 0:
				print 'Problem not found'
				showUsage(problems, mutators)
				exit(1)

		for problem in problems_to_process:
			# problem.verbose = True
			problem.programs_filter = lambda p: True
			problem.validate_programs()

			if len(sys.argv) >= 3:
				exp_name = sys.argv[2]
				if exp_name == "time" and len(sys.argv) > 3:
					size = int(sys.argv[3])
					problem.programs_filter = lambda p: 'minified' not in p
					problem.measure_time(size)
				else:
					for m in mutators:
						if m.get_short_name() == exp_name:
							problem.mutators = [m]
							if m.get_short_name() == "char":
								problem.programs_filter = lambda p: 'minified' in p
							else:
								problem.programs_filter = lambda p: 'minified' not in p
							problem.latex_output = True
							problem.test_mutants()
