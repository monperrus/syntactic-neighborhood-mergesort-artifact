from ..problem_base import *

from lcs import *
from random import choice
from string import ascii_lowercase

class LCS(ProblemBase):

	def __init__(self):
		self.base_dir = './problems/lcs/'
		ProblemBase.__init__(self)

	def get_short_name(self):
		return 'lcs'

	def get_full_name(self):
		return 'LCS'

	def generate_tests(self, count = None):
		print 'Generating %s tests' % self.get_full_name()
		self.tests = {}
		for i in range(count if count else self.test_suite_size):
			# generate
			str1 = ''.join(choice(ascii_lowercase) for i in range(self.test_size))
			str2 = ''.join(choice(ascii_lowercase) for i in range(self.test_size))
			test = str1 + " " + str2
			expected = lcs(str1, str2)
			# store
			self.tests[test] = expected

	def get_tests_for_program(self, program):
		return self.tests
