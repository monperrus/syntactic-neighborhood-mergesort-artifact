from utils.module_utils import *

modules = [v for k,v in import_submodules(__name__).iteritems() if k.endswith("problem")]
classes = [classes_in_module(mod)[0] for mod in modules]
problems = [c() for c in classes]
# print problems
