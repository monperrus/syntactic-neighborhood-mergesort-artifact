import sys

def quickSort(arr):
    less = []
    pivotList = []
    more = []
    if len(arr) <= 1:
        return arr
    else:
        pivot = arr[0]
        for i in arr:
            if i < pivot:
                less.append(i)
            elif i > pivot:
                more.append(i)
            else:
                pivotList.append(i)
        less = quickSort(less)
        more = quickSort(more)
        return less + pivotList + more

if __name__ == "__main__":
    input = []
    for i in range(1, len(sys.argv)):
        input.append(int(sys.argv[i]))

    res = quickSort(input)
    print(" ".join(str(res)[1:-1].split(', ')))
