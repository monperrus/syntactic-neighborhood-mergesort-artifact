import System.Environment

quicksort [] = []
quicksort (x:xs) = quicksort [y | y <- xs, y < x] ++ [x] ++ quicksort [y | y <- xs, y >= x]

main = do
    list <- getArgs
    putStrLn (show (quicksort (read (head list) :: [Integer])))
