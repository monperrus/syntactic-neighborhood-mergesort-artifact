import java.util.List;
import java.util.ArrayList;

public class quicksort {
    public static void main(String[] args) {
        int n = args.length;
        List<Integer> input = new ArrayList<Integer>();
        for (int i = 0; i < n; i++) {
            input.add(Integer.valueOf(args[i]));
        }
        List<Integer> sorted = quickSort(input);
        for (int i = 0; i < n - 1; i++) {
            System.out.print(sorted.get(i));
            System.out.print(" ");
        }
        System.out.print(sorted.get(n - 1));
    }

    public static List<Integer> quickSort(List<Integer> arr) {
        if (!arr.isEmpty()) {
            Integer pivot = arr.get(0);
            List<Integer> less = new ArrayList<Integer>();
            List<Integer> pivotList = new ArrayList<Integer>();
            List<Integer> more = new ArrayList<Integer>();

            for (Integer i: arr) {
                if (i.compareTo(pivot) < 0)
                less.add(i);
                else if (i.compareTo(pivot) > 0)
                more.add(i);
                else
                pivotList.add(i);
            }

            less = quickSort(less);
            more = quickSort(more);

            less.addAll(pivotList);
            less.addAll(more);
            return less;
        }
        return arr;
    }
}
