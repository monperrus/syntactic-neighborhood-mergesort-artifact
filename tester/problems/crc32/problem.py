from ..problem_base import *

from numpy import random
from numpy import array
import zlib
from random import choice
from string import ascii_lowercase

class CRC_32(ProblemBase):

	def __init__(self):
		self.base_dir = './problems/crc32/'
		ProblemBase.__init__(self)

	def get_short_name(self):
		return 'crc-32'

	def get_full_name(self):
		return 'CRC-32'

	def generate_tests(self, count = None):
		print 'Generating %s tests' % self.get_full_name()
		self.tests = {}
		for i in range(count if count else self.test_suite_size):
			# generate
			test = ''.join(choice(ascii_lowercase) for i in range(self.test_size))
			expected = hex(zlib.crc32(test)).upper()[2:]

			# store
			self.tests[test] = expected

	def get_tests_for_program(self, program):
		return self.tests
