import System.Environment

merge [] ys = ys
merge xs [] = xs
merge xs@(x:xt) ys@(y:yt) | x <= y = x : merge xt ys | otherwise = y : merge xs yt

split (x:y:zs) = let (xs,ys) = split zs in (x:xs,y:ys)
split [x] = ([x],[])
split [] = ([],[])

mergesort [] = []
mergesort [x] = [x]
mergesort xs = let (as,bs) = split xs in merge (mergesort as) (mergesort bs)

main = do
    list <- getArgs
    putStrLn (show (mergesort (read (head list) :: [Integer])))
