from ..problem_base import *

from numpy import random
from numpy import array

class Mergesort(ProblemBase):

	def __init__(self):
		self.base_dir = './problems/mergesort/'
		self.test_range = 30
		ProblemBase.__init__(self)

	def get_short_name(self):
		return 'mergesort'

	def get_full_name(self):
		return 'Mergesort'

	def generate_tests(self, count = None):
		print 'Generating %s tests' % self.get_full_name()
		self.tests = {}
		self.tests_haskell = {}
		for i in range(count if count else self.test_suite_size):
			# generate
			test = random.randint(self.test_range, size=self.test_size)
			sorted = 1*test
			sorted.sort()
			# get string from test and expected
			# we are gonna remove the brackets in the string ('[',']'), and
			# convert each multiple space into one.
			str_test = " ".join(str(test.tolist())[1:-1].split(', '))
			str_expected = " ".join(str(sorted.tolist())[1:-1].split(', '))
			self.tests[str_test] = str_expected

			input_haskell = "[" + str_test.replace(' ', ',') + "]"
			expected_haskell = "[" + str_expected.replace(' ', ',') + "]"
			self.tests_haskell[input_haskell] = expected_haskell

	def get_tests_for_program(self, program):
		if is_haskell_program(program):
			return self.tests_haskell
		else:
			return self.tests
