import java.util.*;

public class lzw {

    /** Compress a string to a list of output symbols. */
    public static List<Character> compress(String uncompressed) {
        // Build the dictionary.
        int dictSize = 256;
        Map<String,Integer> dictionary = new HashMap<String,Integer>();
        for (int i = 0; i < 256; i++)
            dictionary.put("" + (char)i, i);

        String w = "";
        List<Character> result = new ArrayList<Character>();
        for (char c : uncompressed.toCharArray()) {
            String wc = w + c;
            if (dictionary.containsKey(wc))
                w = wc;
            else {
                result.add((char)((int)dictionary.get(w)));
                // Add wc to the dictionary.
                dictionary.put(wc, dictSize++);
                w = "" + c;
            }
        }

        // Output the code for w.
        if (!w.equals(""))
            result.add((char)((int)dictionary.get(w)));
        return result;
    }

    public static void main(String[] args) {
        List<Character> compressed = compress(args[0]);
        for(char c : compressed) {
            System.out.print(c);
        }
    }
}
