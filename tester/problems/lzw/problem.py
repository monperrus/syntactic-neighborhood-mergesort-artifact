from ..problem_base import *

from lzw import *
from numpy import random
from numpy import array
from random import choice
from string import ascii_lowercase

class LZW(ProblemBase):

	def __init__(self):
		self.base_dir = './problems/lzw/'
		ProblemBase.__init__(self)

	def get_short_name(self):
		return 'lzw'

	def get_full_name(self):
		return 'LZW'

	def generate_tests(self, count = None):
		print 'Generating %s tests' % self.get_full_name()
		self.tests = {}
		for i in range(count if count else self.test_suite_size):
			# generate
			test = ''.join(choice(ascii_lowercase) for i in range(self.test_size))
			asdf = compress(test)
			print asdf
			expected = ''.join(asdf)
			# store
			self.tests[test] = expected

	def get_tests_for_program(self, program):
		return self.tests
