#include <stdio.h>
#include <math.h>
#include <complex.h>
#include <string.h>

double PI;
typedef double complex cplx;

void _fft(cplx buf[], cplx out[], int n, int step) {
    int i;
	if (step < n) {
		_fft(out, buf, n, step * 2);
		_fft(out + step, buf + step, n, step * 2);

		for (i = 0; i < n; i += 2 * step) {
			cplx t = cexp(-I * PI * i / n) * out[i + step];
			buf[i / 2]     = out[i] + t;
			buf[(i + n)/2] = out[i] - t;
		}
	}
}

void fft(cplx buf[], int n) {
	cplx out[n];
    int i;
	for (i = 0; i < n; i++){
        out[i] = buf[i];
    }

	_fft(buf, out, n, 1);
}


void show(cplx buf[]) {
    int i;
	for (i = 0; i < 8; i++)
		if (!cimag(buf[i]))
			printf("%5.3f ", creal(buf[i]));
		else
			printf("%5.3f ", sqrt(pow(creal(buf[i]),2) + pow(cimag(buf[i]),2)));
}

int main( int argc, char *argv[]) {
	PI = atan2(1, 1) * 4;

    char *input = argv[1];
    int str_len = strlen(input);

    int n = 0;
    int i;
    // count white spaces
    for (i = 0; i < str_len; i++) {
        if (input[i] == ' ') {
            ++n;
        }
    }
    ++n; // for the last float value in string

    // read input into buffer
    char *delim = " "; // input separated by spaces
    char *token = NULL;
    cplx buf[n];

    i = 0;
    for (token = strtok(input, delim); token != NULL; token = strtok(NULL, delim)) {
        double value;
        sscanf(token, "%lf",&value);
        buf[i] = value;
        ++i;
    }

    fft(buf, n);
	show(buf);

	return 0;
}
