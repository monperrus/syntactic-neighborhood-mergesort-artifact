from helper_base import *

from utils.command import TimerTask
from utils.file_utils import *

class Python_Helper(HelperBase):
	def __init__(self, program, base_dir):
		HelperBase.__init__(self, program)

	def get_short_name(self):
		return 'python'

	def get_full_name(self):
		return 'Python'

	@staticmethod
	def is_valid_program(program):
	    return program.endswith(".py") and not program.endswith("__init__.py")  and not program.endswith("problem.py")

	def pre_mutants_testing(self):
		pass

	def pos_mutants_testing(self):
		pass

	def build(self, mutant = None):
		return True

	def run(self, input, mutant = None):
		if mutant:
			src = 'aux.py'
			write_to_file(src, mutant)
		else:
			src = self.program

		if self.verbose: print 'Testing with: ' + input

		run_cmd = TimerTask("python3 " + src + " " + input, timeout=2)
		proc = run_cmd.run(stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		out, err = proc.communicate()
		proc.wait()

		if proc.returncode == -9:
			err = "ERROR: Timeout"

		if proc.returncode != 0:
			if self.verbose: print err
			if self.logging:
				self.interpreter_error_log.append(err)
			return None
		else:
			return out[:-1] # remove new-line character at the end

	def time(self, input):
		if self.verbose: print 'Timing with: ' + input

		run_cmd_str = "python " + self.program + " " + input
		time_cmd = 'bash -c \"TIMEFORMAT=\'%9E\'; time ('+ run_cmd_str + '; echo \'\')\"'

		time_cmd = TimerTask(time_cmd, timeout=1)
		proc = time_cmd.run(stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		out, err = proc.communicate(str(input))
		proc.wait()

		if self.verbose: print err
		return err
