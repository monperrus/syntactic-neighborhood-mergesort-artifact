import os
from utils.module_utils import *
from helper_base import HelperBase

def get_language_helper(program, base_dir):
    current_dir = os.path.dirname(os.path.abspath(__file__))
    helpers = import_plugins(current_dir, base_class=HelperBase)
    for h in helpers:
        if h.is_valid_program(program):
            return h(program, base_dir)
    raise ValueError(program + ' has an unknown extension')

def has_known_extension(program):
    current_dir = os.path.dirname(os.path.abspath(__file__))
    helpers = import_plugins(current_dir, base_class=HelperBase)
    for h in helpers:
        if h.is_valid_program(program):
            return True
    return False
