import sys
import abc
import subprocess
from utils.command import TimerTask
from utils.file_utils import *

class HelperBase(object):
    def __init__(self, program):
        self.program = program
        if not self.is_valid_program(program):
            raise ValueError('Invalid program for %s helper' % self.get_full_name())

        self.verbose = False
        self.logging = True
        self.latex_output = False
        self.setup_logs()

        self.tests = {}

    @abc.abstractmethod
    def get_short_name(self):
		"""Should return a short name of the programming language, mainly used to run the experiments."""

    @abc.abstractmethod
    def get_full_name(self):
        """Should return a full name of the programming language, mainly used to print verbose output."""

    @abc.abstractmethod
    def is_valid_program(self, program):
         """Should return wheter the program name is or not valid for the current helper."""

    @abc.abstractmethod
    def build(self, mutant = None):
         """Should build the original or mutated program if needed, and return wheter it was successful or not."""

    @abc.abstractmethod
    def run(self, input, mutant = None):
        """Should run the original or mutated program and return the output.
        In case of Time Out or return code different to 0, it should return None as output"""

    @abc.abstractmethod
    def pre_mutants_testing(self):
         """Convenience method to allow helpers do some tasks before starting to test mutants."""

    @abc.abstractmethod
    def pos_mutants_testing(self):
         """Convenience method to allow helpers do some tasks after finished testing mutants."""

    def setup_logs(self):
        self.MAX_OUTPUT = 2000

        self.compiler_error_log = []
        self.compiler_error_log_file = "logs/%s_compiler_log.txt" % self.get_short_name()
        self.compiler_error_recap_file = "logs/%s_compiler_log_recap.txt" % self.get_short_name()

        self.interpreter_error_log = []
        self.interpreter_error_log_file = "logs/%s_interpreter_log.txt" % self.get_short_name()
        self.interpreter_error_recap_file = "logs/%s_interpreter_log_recap.txt" % self.get_short_name()

        self.runtime_error_log = []
        self.runtime_error_log_file = "logs/%s_runtime_log.txt" % self.get_short_name()
        self.runtime_error_recap_file = "logs/%s_runtime_log_recap.txt" % self.get_short_name()

        self.incorrect_output_log = []
        self.incorrect_output_log_file = "logs/%s_incorrect_output_log.txt" % self.get_short_name()
        self.incorrect_output_recap_file = "logs/%s_incorrect_output_log_recap.txt" % self.get_short_name()

        self.mutants_log_file = "logs/%s_mutants_log.txt" % self.get_short_name()
        self.mutants_recap_file = "logs/%s_mutants_log_recap.txt" % self.get_short_name()

    def measure_runtime(self):
        self.build()

        totalSecs = float(0)
        for input in self.tests.iterkeys():
            result = self.time_c(input)
            totalSecs += float(result)

        totalSecs = str(totalSecs)

        timeParts = str(totalSecs).split('.')
        totalSecs = int(timeParts[0])
        remaining = timeParts[1]

        totalSecs, sec = divmod(totalSecs, 60)
        hr, min = divmod(totalSecs, 60)
        print "Testing %d instances for program %s took: %d:%02d:%02d.%s" % (len(self.tests), self.program, hr, min, sec, remaining)
        return

    def validate(self):
        self.verbose = True
        if not self.build():
            return False
        for input, expected in self.tests.iteritems():
            output = self.run(input)
            if not output:
                return False
            if not self.is_output_equal_expected(output, expected):
                return False
        return True

    def test_mutants(self, mutants):
        equivalent_mutants_numbers = []
        self.pre_mutants_testing()
        for i in xrange(len(mutants)):
            print 'Mutant number: %d/'%i,
            if not self.verbose:
                print '%d\r'%len(mutants),
            else:
                print '%d\n'%len(mutants)

            if not self.build(mutant = mutants[i]):
                if self.verbose:
                    print '%s mutant failed build' % self.get_full_name()
                continue
            passed_all = True
            for input, expected in self.tests.iteritems():
                output = self.run(input, mutant = mutants[i])
                if not output and output != "":
                    passed_all = False
                    break
                if not self.is_output_equal_expected(output, expected):
                    passed_all = False
                    break
            if passed_all:
                equivalent_mutants_numbers.append(i)

        self.pos_mutants_testing()

        total = len(mutants)
        print '- For program {0}, there were {1} mutants:'.format(self.program, total)
        print '  - The numbers of the equivalent mutants are: {0}'.format(equivalent_mutants_numbers)
        print '  - {0} ({1:.4f}%) passed all tests.'.format(len(equivalent_mutants_numbers), len(equivalent_mutants_numbers)*100/float(total))
        print '  - {0} ({1:.4f}%) had incorrect output for a test.'.format(len(self.incorrect_output_log), len(self.incorrect_output_log)*100/float(total))
        print '  - {0} ({1:.4f}%) had runtime errors.'.format(len(self.runtime_error_log), len(self.runtime_error_log)*100/float(total))
        print '  - {0} ({1:.4f}%) had interpreter errors.'.format(len(self.interpreter_error_log), len(self.interpreter_error_log)*100/float(total))
        print '  - {0} ({1:.4f}%) had compiler errors.'.format(len(self.compiler_error_log), len(self.compiler_error_log)*100/float(total))

        # warning check
        total_error_logs = len(self.incorrect_output_log) + len(self.runtime_error_log) + len(self.interpreter_error_log) + len(self.compiler_error_log)
        if total_error_logs + len(equivalent_mutants_numbers) != total:
            print 'WARNING: sum of total error logs is {0} and there is {1} equivalent mutants, but the total mutants is {2}'.format(total_error_logs, len(equivalent_mutants_numbers), total)

        if self.logging:
            # Mark equivalent mutants
            for i in equivalent_mutants_numbers:
                mutants[i] = "E" + mutants[i]

            # write logs to disk
            write_log(self.compiler_error_log_file, self.compiler_error_log)
            write_log(self.interpreter_error_log_file, self.interpreter_error_log)
            write_log(self.runtime_error_log_file, self.runtime_error_log)
            write_log(self.incorrect_output_log_file, self.incorrect_output_log)
            write_log(self.mutants_log_file, mutants)

            # Generate error recaps
            generate_recap(self.compiler_error_log_file, self.compiler_error_recap_file)
            generate_recap(self.interpreter_error_log_file, self.interpreter_error_recap_file)
            generate_recap(self.runtime_error_log_file, self.runtime_error_recap_file)
            generate_recap(self.incorrect_output_log_file, self.incorrect_output_recap_file)

        if self.latex_output:
            print '- Latex output:'
            print "Language & Program & Compiler err. & Interpreter err. & Runtime err. & Incorrect out. & Correct out. & Total \\\\ \hline"
            print "{0} & {1} & {2} ({3:.2f}\\%) & {4} ({5:.2f}\\%) & {6} ({7:.2f}\\%) & {8} ({9:.2f}\\%) & {10} ({11:.2f}\\%) & {12} (100\\%) \\\\ \hline".format(self.get_full_name(), self.program, len(self.compiler_error_log), len(self.compiler_error_log)*100/float(total), len(self.interpreter_error_log), len(self.interpreter_error_log)*100/float(total), len(self.runtime_error_log), len(self.runtime_error_log)*100/float(total), len(self.incorrect_output_log), len(self.incorrect_output_log)*100/float(total), len(equivalent_mutants_numbers), len(equivalent_mutants_numbers)*100/float(total), total)
            print ''

    #  ---------------------- AUXILIAR FUNCTIONS -------------------------------
    def is_output_equal_expected(self, output, expected):
        # print output
        # print expected
        str_output = str(output)
        str_expected = str(expected)
        if str_output == str_expected:
            if self.verbose: print 'RIGHT ANSWER'
            return True
        else:
            if self.verbose:
                print 'WRONG ANSWER'
                if len(str_output) <= self.MAX_OUTPUT:
                    print 'output is: \"' + str_output + '\"'
                else:
                    print 'output is: \"' + str_output[:self.MAX_OUTPUT/2] + '[...]' + str_output[len(str_output) - self.MAX_OUTPUT/2:len(str_output)] + '\"'
                print 'but expected was: \"' + str_expected + '\"'
            if self.logging:
                if len(str_output) <= self.MAX_OUTPUT:
                    self.incorrect_output_log.append('output: \"' + str_output + '\"\nexpected: \"' + str_expected + '\"\n')
                else:
                    self.incorrect_output_log.append('output: \"' + str_output[:self.MAX_OUTPUT/2] + '[...]' + str_output[len(str_output) - self.MAX_OUTPUT/2:len(str_output)] + '\"\nexpected: \"' + str_expected + '\"\n')
            return False

    def move_logs(self, folder):
        mv_cmd = TimerTask('mv logs/{0}_*.txt '.format(self.get_short_name()) + folder + ' 2>/dev/null')
        mv_cmd.run()

    def clean_logs(self):
        self.interpreter_error_log = []
        self.incorrect_output_log = []
        clean_cmd = TimerTask('rm -f logs/%s_*.txt' % self.get_short_name())
        clean_cmd.run()
