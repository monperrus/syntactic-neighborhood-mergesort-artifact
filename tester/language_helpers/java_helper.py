from helper_base import *

import shutil
from utils.command import TimerTask
from utils.file_utils import *

class Java_Helper(HelperBase):
	def __init__(self, program, base_dir):
		HelperBase.__init__(self, program)

		self.base_dir = base_dir
		# drop '.java' and append '.class'
		self.java_object = self.program.split('/')[-1][:-5]
		self.program_backup = self.program[:-5] + '.bak'

	def get_short_name(self):
		return 'java'

	def get_full_name(self):
		return 'Java'

	@staticmethod
	def is_valid_program(program):
	    return program.endswith(".java")

	def pre_mutants_testing(self):
		shutil.copy(self.program, self.program_backup)

	def pos_mutants_testing(self):
		shutil.copy(self.program_backup, self.program)

	def build(self, mutant = None):
		if mutant:
			write_to_file(self.program, mutant)
		# try to compile file
		build_cmd_str = "javac " + self.program
		build_cmd = TimerTask(build_cmd_str, timeout = 3)
		proc = build_cmd.run(stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		out, err = proc.communicate()
		proc.wait()
		output = out + err
		if proc.returncode:
			if self.verbose:
				print '{0} failed build'.format(self.program)
				print output
				print ''
			if self.logging: self.compiler_error_log.append(output)
			return False
		else:
			return True

	def run(self, input, mutant = None):
		if self.verbose: print 'Testing with: ' + input
		run_cmd_str = "java -cp " + self.base_dir + "/ " + self.java_object + " " + input
		run_cmd = TimerTask(run_cmd_str, timeout=1)
		proc = run_cmd.run(stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		out, err = proc.communicate()
		proc.wait()

		if proc.returncode == -9:
			err = "ERROR: Timeout"

		if proc.returncode != 0:
			if self.verbose: print err
			if self.logging: self.runtime_error_log.append(err)
			return None
		else:
			return out

	def time(self, input):
		if self.verbose: print 'Timing with: ' + str(input)

		run_cmd_str = "java -cp " + self.base_dir + "/ " + self.java_object + " " + str(input)
		time_cmd = 'bash -c \"TIMEFORMAT=\'%9E\'; time ('+ run_cmd_str + '; echo \'\')\"'

		time_cmd = TimerTask(time_cmd, timeout=1)
		proc = time_cmd.run(stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		out, err = proc.communicate()
		proc.wait()

		if self.verbose: print err
		return err
