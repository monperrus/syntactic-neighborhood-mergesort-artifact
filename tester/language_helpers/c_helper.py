from helper_base import *

from utils.command import TimerTask

c_object = "aux.o"

class C_Helper(HelperBase):
	def __init__(self, program, base_dir):
		HelperBase.__init__(self, program)

	def get_short_name(self):
		return 'c'

	def get_full_name(self):
		return 'C'

	@staticmethod
	def is_valid_program(program):
	    return program.endswith(".c")

	def pre_mutants_testing(self):
		pass

	def pos_mutants_testing(self):
		pass

	def build(self, mutant = None):
		if mutant:
			src = 'aux.c'
			write_to_file(src, mutant)
		else:
			src = self.program
		# try to compile file
		build_cmd = TimerTask("gcc " + src + " -o " + c_object + " -lm", timeout = 2)
		proc = build_cmd.run(stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		out, err = proc.communicate()
		proc.wait()
		output = out + err

		if proc.returncode:
			if self.verbose:
				print '{0} failed build'.format(self.program)
				print output
				print ''
			if self.logging: self.compiler_error_log.append(output)
			return False
		else:
			return True

	def run(self, input, mutant = None):
		if self.verbose: print 'Testing with: ' + input

		run_cmd = TimerTask("./" + c_object + ' ' + input, timeout=2)
		proc = run_cmd.run(stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		out, err = proc.communicate()
		proc.wait()

		if proc.returncode == -9:
			err = "ERROR: Timeout"

		if proc.returncode != 0:
			if self.verbose: print err
			if self.logging: self.runtime_error_log.append(err)
			return None
		else:
			return out

	def time(self, input):
		if self.verbose: print 'Timing with: ' + str(input)

		run_cmd_str = "./" + c_object
		time_cmd = 'bash -c \"TIMEFORMAT=\'%9E\'; time ('+ run_cmd_str + '; echo \'\')\"'

		time_cmd = TimerTask(time_cmd, timeout=1)
		proc = time_cmd.run(stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		out, err = proc.communicate(str(input))
		proc.wait()

		print err
		return err
