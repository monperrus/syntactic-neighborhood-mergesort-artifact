import os
import re
import sys
from mutator_helper import *

keyboard_rows = [
	"1234567890-=\\",
	"qwertyuiop[]",
	"asdfghjkl;'",
	"zxcvbnm,./"
	]

keyboard_shifted_rows = [
	"!@#$%^&*()_+|",
	"QWERTYUIOP\{\}",
	"ASDFGHJKL:\"",
	"ZXCVBNM<>?"
	]

class QwertyKeyboardMutator(object):
	def __init__(self):
		self.keyboard_mutations = self.generate_typos_map(keyboard_rows)
		self.keyboard_shifted_mutations = self.generate_typos_map(keyboard_shifted_rows)

	def get_short_name(self):
		return 'qwerty'

	def get_all_mutants(self, program):
		mutants = []
		source_code = open(program).read().split('\n')

		# explore all lines
		for line in xrange(len(source_code)):
			# do not mutate preprocessor, assert statements or empty lines
			line_strip = source_code[line].strip()
			if is_mutable_line(line_strip):
				# explore all chars for one line
				for i in xrange(len(source_code[line])):
					original_char = source_code[line][i]
					if original_char != ' ' and self.is_known_char(original_char):
						posible_mutations = []
						if self.is_shiftted_char(original_char):
							posible_mutations = self.keyboard_shifted_mutations[original_char]
						else:
							posible_mutations = self.keyboard_mutations[original_char]

						for new_char in posible_mutations:
							mutant = list(source_code)
							mutant[line] = mutant[line][:i] + new_char + mutant[line][i+1:]
							mutants.append(mutant)

		# return each mutant as one string
		for i in xrange(len(mutants)):
			aux = ""
			for j in xrange(len(mutants[i])):
				aux += mutants[i][j] + '\n'
			mutants[i] = aux

		return mutants

	def write_to_file (self, mutant_file_name, source_code, mutated_line_number, mutated_line):
		output_file = open(mutant_file_name, "w")
		for i in xrange(0,len(source_code)) :
			if i == mutated_line_number :
				#output_file.write("/* XXX: original code was : "+source_code[i]+" */\n")
				output_file.write(mutated_line+"\n")
			else :
				output_file.write(source_code[i]+"\n")

		output_file.close()

	def generate_typos_map(self, keyboard_rows):
		# Build qwerty keyboard mutations
		mutations = {}
		rows_count = len(keyboard_rows)
		for row_pos in xrange(rows_count):
			# figure out previous and next row
			previous_row_pos = max(row_pos - 1, 0)
			next_row_pos = min(row_pos + 1, rows_count - 1)

			row_size = len(keyboard_rows[row_pos])
			previous_row_size = len(keyboard_rows[previous_row_pos])
			next_row_size = len(keyboard_rows[next_row_pos])
			# analize each letter of current row
			for char_pos in xrange(row_size):
				char = keyboard_rows[row_pos][char_pos]
				# figure out contiguos chars in previous, next and current row
				if previous_row_pos != row_pos:
					contiguos_chars_previous_row = [keyboard_rows[previous_row_pos][i] for i in xrange(previous_row_size) if i >= char_pos - 1 and i <= char_pos + 1]
				else:
					contiguos_chars_previous_row = []

				if next_row_pos != row_pos:
					contiguos_chars_next_row = [keyboard_rows[next_row_pos][i] for i in xrange(next_row_size) if i >= char_pos - 1 and i <= char_pos + 1]
				else:
					contiguos_chars_next_row = []

				contiguos_chars_current_row = [keyboard_rows[row_pos][i] for i in xrange(row_size) if i >= char_pos - 1 and i <= char_pos + 1 and i != char_pos]
				mutations[char] = contiguos_chars_previous_row + contiguos_chars_current_row + contiguos_chars_next_row
		return mutations

	def is_shiftted_char(self, char):
		for row in keyboard_shifted_rows:
			if char in row:
				return True
		return False

	def is_normal_char(self, char):
		for row in keyboard_rows:
			if char in row:
				return True
		return False

	def is_known_char(self, char):
		return self.is_shiftted_char(char) or self.is_normal_char(char)
