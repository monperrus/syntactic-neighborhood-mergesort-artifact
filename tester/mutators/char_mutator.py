import os
import re
import sys
from mutator_helper import *

# symbols = "!\"#$%&'()*+,-./:;<=>?@{|}~[\]^_`"
symbols = "!#$%&'()*+,-./:;<=>?@{|}~[\]^_`"
numbers = "0123456789"
letters = "abcdefghijklmnopqrstuvwxyz"
upper_letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

printable_chars = symbols + numbers + letters

class CharMutator(object):
	def get_short_name(self):
		return 'char'

	def get_all_mutants(self, program):
		mutants = []
		source_code = open(program).read().split('\n')

		# explore all lines
		for line in xrange(len(source_code)):
			# do not mutate preprocessor, assert statements or empty lines
			line_strip = source_code[line].strip()
			if is_mutable_line(line_strip):
				# explore all chars for one line
				for i in xrange(len(source_code[line])):
					if source_code[line][i] == ' ':
						# avoid whitespaces
						continue
					original_char = source_code[line][i]
					for j in xrange(len(printable_chars)):
						if printable_chars[j] == original_char:
							# avoid replacing original character by the same character
							continue
						mutant = list(source_code)
						mutant[line] = mutant[line][:i] + printable_chars[j] + mutant[line][i+1:]
						mutants.append(mutant)

		# return each mutant as one string
		for i in xrange(len(mutants)):
			aux = ""
			for j in xrange(len(mutants[i])):
				aux += mutants[i][j] + '\n'
			mutants[i] = aux

		return mutants

	def write_to_file (self, mutant_file_name, source_code, mutated_line_number, mutated_line):
		output_file = open(mutant_file_name, "w")
		for i in xrange(0,len(source_code)) :
			if i == mutated_line_number :
				#output_file.write("/* XXX: original code was : "+source_code[i]+" */\n")
				output_file.write(mutated_line+"\n")
			else :
				output_file.write(source_code[i]+"\n")

		output_file.close()
