import os
import re
import sys
from mutator_helper import *

class FakeMutator(object):
	def get_short_name(self):
		return 'fake'

	def get_all_mutants(self, program):
		mutants = []
		source_code = open(program).read().split('\n')
		mutants.append(list(source_code))

		# return each mutant as one string
		for i in xrange(len(mutants)):
			aux = ""
			for j in xrange(len(mutants[i])):
				aux += mutants[i][j] + '\n'
			mutants[i] = aux

		return mutants
